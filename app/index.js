import 'babel-polyfill';
import 'react-notifications/src/notifications.scss';
import './assets/favicon.ico';
import React from 'react'
import ReactDOM from 'react-dom'
import routes from './config/routes'

ReactDOM.render(routes, document.getElementById('app'))