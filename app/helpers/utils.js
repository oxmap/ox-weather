const daysMap = {
  "0":"Вс",
  "1":"Пн",
  "2":"Вт",
  "3":"Ср",
  "4":"Чт",
  "5":"Пт",
  "6":"Сб"
}
const monthMap = {
  "0":"Января",
  "1":"Февраля",
  "2":"Марта",
  "3":"Апреля",
  "4":"Мая",
  "5":"Июня",
  "6":"Июля",
  "7":"Августа",
  "8":"Сентября",
  "9":"Октября",
  "10":"Ноября",
  "11":"Декабря",  
}

const COMPASS_POINTS = [
  { symbol: 'C', name: 'Север'},
  { symbol: 'СВ', name: 'Северо-Восток'},
  { symbol: 'З', name: 'Запад'},
  { symbol: 'ЮВ', name: 'Юго-Восток'},
  { symbol: 'Ю', name: 'Юг'},
  { symbol: 'ЮЗ', name: 'Юго-Запад'},
  { symbol: 'З', name: 'Запад'},
  { symbol: 'СЗ', name: 'Северо-Запад'},
];

export function getDate(unixTimestmap) {
  const date = new Date(unixTimestmap * 1000);
  return daysMap[date.getDay()];
}

export function getMonthDay(unixTimestmap) {
  const date = new Date(unixTimestmap * 1000);
  return `${date.getUTCDate()} ${monthMap[date.getMonth()]}`;
}


export function convertToCelcius(kelvin) {
  return Math.round(kelvin - 273.15, 0)
}

export function convertToPoint(degrees) {
  let idx = Math.round(degrees / 45);
  // 360 === 0 aka North
  if (idx === COMPASS_POINTS.length) {
      idx = 0;
  }
  return COMPASS_POINTS[idx];
}

function getUnixTimePrivate(dt) {
  const date = new Date(dt * 1000);
  const hours = "0" + date.getHours();
  const minutes = "0" + date.getMinutes();
  return [hours, minutes];
}

export function convertUnixTime(dt) {
  const [hours, minutes] = getUnixTimePrivate(dt);
  // hh:mm format
  return hours.substr(-2) + ':' + minutes.substr(-2);
}

export function getDayDuration(rise, set) {
  const riseDate = new Date(rise * 1000);
  const setDate = new Date(set * 1000);
  let diff = Math.abs(setDate - riseDate);

  const hh = Math.floor(diff/1000/60/60);
  diff -= hh*1000*60*60;
  const mm = Math.floor(diff/1000/60);
  
  return `${hh} ч ${mm} мин`;
}