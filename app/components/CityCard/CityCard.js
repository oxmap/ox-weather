import React from 'react'
import { WeatherForecastList, 
   WeatherTemperature } from '../WeatherCity/WeatherElements'
import { convertToPoint, convertUnixTime, getDayDuration, convertToCelcius } from '../../helpers/utils'
import { Link } from 'react-router'
import FontAwesome from 'react-fontawesome'
import WeatherIcon from 'react-icons-weather'
import './CityCard.scss'

class CityCard extends React.Component {
  removeCity(event, city) {
    // Remove city element from the DOM
    event.persist()
    event.target.parentNode.classList.add('removed')
    setTimeout(function() {
        event.target.parentNode.classList.add('hide')
    }, 500);
    this.props.removeCity(city)
  }

  renderAdditionals(data) {
    return (
      <div className="weather-additionals">
        <div className="addictional-section">
          <span>Влажность</span> 
          <span className="additional-value">{`${data.weatherHumidity}%`}</span> 
        </div>
        <div className="addictional-section center">
          <span>Давление</span> 
          <span className="additional-value">{`${data.weatherPressure} мм рт. ст.`}</span>
        </div>
        <div className="addictional-section">
          <span>Ветер</span> 
          <span className="additional-value">{`${data.weatherWindSpeed} м/с`} {`${convertToPoint(data.weatherWindDeg)['symbol']}`}</span>
        </div>
      </div>  
    )
  }

  renderSunSet(data) {
    return (
    <div className="weather-sun">
      <div className='iconSet'>
        <i className="icon icon_size_24 icons_sunrise" aria-hidden="true" data-width="24"></i>
        <div className='sunTitle'>{`${convertUnixTime(data.weatherSunRise)}`} </div>
      </div>

      <div className='iconSet'>
        <div className='sunTitle'>Световой день</div>
        <div className='sunTitle'>{`${getDayDuration(data.weatherSunRise, data.weatherSunSet)}`} </div>
      </div>

      <div className='iconSet'>
        <i className="icon icon_size_24 icons_sunset" aria-hidden="true" data-width="24"></i>
        <div className='sunTitle'>{`${convertUnixTime(data.weatherSunSet)}`} </div>
      </div>
    </div>
    )
  }

  render() {
    return (
      <li className="city-weather col-xs-12 col-sm-4">
        <FontAwesome 
          name="trash" 
          className="remove-city" 
          onClick={(event) => this.removeCity(event, this.props.cityData.cityFullName)}
        />

        <div className="inner-container weather-card">
          <WeatherIcon className="weather-icon" name="owm" iconId={this.props.cityData.icon.toString()}/>
          <p className="weather-description">{this.props.cityData.weatherDescription}</p>
          <h1>{this.props.cityData.cityName}</h1>
          <div className="weather-temperature-card">
            <span>{`${convertToCelcius(this.props.cityData.weatherTemp)}°`}</span>
          </div>
          {this.renderSunSet(this.props.cityData)}
          {this.renderAdditionals(this.props.cityData)}
          <Link to={`/weather-city/?city=${this.props.cityData.cityName}`}>
            <div className="detail-button">
              <button><span>Подробнее</span></button>
            </div>
          </Link>
        </div>
      </li>
    )
  }
}

export default CityCard