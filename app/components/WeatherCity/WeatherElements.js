import React from 'react'
import FontAwesome from 'react-fontawesome'
import WeatherIcon from 'react-icons-weather'
import { getDate, getMonthDay, convertToCelcius, convertToPoint } from '../../helpers/utils'

export function WeatherForecastList({forecastList}) {
  return (
    <div className="forecast-container">
      <ul>
        {forecastList.slice(1).map(function(element, i){
          return (
            <li className='forecast-element' key={i}>
              <p className='forecast-day'>{getDate(element.dt)}</p>
              <p className='forecast-dayOfMonth'>{getMonthDay(element.dt)}</p>
              <WeatherIcon className="days-icon" name="owm" iconId={element.weather[0].id.toString()}/>
              <span className="day-temp">{`${convertToCelcius(element.temp.day)}°`}</span>
              <span>{`${convertToCelcius(element.temp.night)}°`}</span>
              <p className='forecast-description'>{element.weather[0].description}</p>
            </li>
          )
        })}
      </ul>
    </div>
)}