import React from 'react'
import { getDate, getMonthDay, convertToCelcius } from '../../helpers/utils'
import { WeatherForecastList } from './WeatherElements'
import WeatherIcon from 'react-icons-weather'
import Loading from '../Loading/Loading'

function WeatherCity(props) {
  return (
    props.isLoading === true
    ? <Loading />
    : <section className={`weather-details`}>
        <div className="details-section">
            <p className='cityTitle'>Погода в {props.cityName}</p>
            <p className='forecast-dayOfMonth'>{getMonthDay(props.dt)}</p>
            <p className='forecast-day'>{getDate(props.dt)}</p>
            <div className="title-temperature">
                <span>{`${convertToCelcius(props.weatherTemp)}°`}</span>
                <WeatherIcon className="weather-icon" name="owm" iconId={props.icon.toString()}/>
            </div>
            <WeatherForecastList forecastList={props.forecast} />
        </div>
        <div className="details-back">
            <img src={props.imgSource} className="flickr_img"></img>
        </div>
    </section>
  )
}

export default WeatherCity