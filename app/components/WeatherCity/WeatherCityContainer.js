import React from 'react'
import { Link } from 'react-router'
import MainContainer from '../Main/MainContainer'
import { getCityCurrentWeather, getForecastWeather } from '../../helpers/apis/owm_api'
import axios from 'axios'
import FontAwesome from 'react-fontawesome'
import WeatherCity from './WeatherCity'
import './Weather.scss'

class WeatherCityContainer extends React.Component {
  constructor () {
    super()
    this.state = {
      cityName: '',
      weatherDescription: '',
      weatherTemp: 0,
      weatherSunRise: 0,
      weatherSunSet: 0,
      weatherHumidity: 0,
      weatherPressure: 0,
      weatherWindSpeed: 0,
      weatherWindDeg: 0,
      forecast: [],
      icon: '800',
      cityPicture: '',
      isLoading: true,
      imgSource: '',
      dt: 0
    }
  }

  setApiPlaceQuery = (city) => {
    return `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=bc1f70e7073ae4e52c636815a2330a33&tags=city%2C+view%2C+${this.state.country}&text=${city}&sort=relevance&accuracy=11&safe_search=1&per_page=1&page=1&format=json&nojsoncallback=1`
  };

  async getWeather(props) {
    const { query } = props;
    try {
      const weather = await getCityCurrentWeather(query.city);
      const forecast = await getForecastWeather(query.city);
      
      this.setState({
        cityName: weather.data.name,
        weatherDescription: weather.data.weather[0].description,
        forecast: forecast.data.list,
        weatherTemp: weather.data.main.temp,
        icon: weather.data.weather[0].id,
        dt: weather.data.dt
      })
    } catch (error) {
      console.warn('Error in WeatherCityContainer')
    }
  }

  getPicture = (city) => {
    return axios.get(this.setApiPlaceQuery(city))
        .then((response) => {
            let imgRef = response.data.photos.photo;
            return imgRef;
        })
        .then((imgRef) => {
            const img = imgRef[0];
            const API_IMG_QUERY = `https://farm${img.farm}.staticflickr.com/${img.server}/${img.id}_${img.secret}_z.jpg`;
            this.setState({imgSource: `${API_IMG_QUERY}`, isLoading: false});
        })
        .catch(function (error) {
            console.error('WeatherCityContainer: ', error);
        });
  };

  componentDidMount() {
    this.setState({isLoading: true});
    // Getting weather and picture info
    this.getWeather(this.props.location);
    this.getPicture(this.props.location.query.city);
  }

  componentWillReceiveProps(newProps) {
    this.getWeather(newProps.location);
    this.getPicture(newProps.location.query.city);
  }

  getName() {
    const fullCityName = this.props.location.query.city;
    return fullCityName.split(',')[0];
  }

  render() {
    return (
      <MainContainer pageName="weatherCityPage">
        <img id="city-background" className="city-background" src={this.state.imgSource}></img>
        <Link to='/'>
          <button className="home-link">
             <FontAwesome name='chevron-left' />
          </button>
        </Link>
        <WeatherCity
          cityName={this.getName()}
          weatherDescription={this.state.weatherDescription}
          icon={this.state.icon}
          dt={this.state.dt}
          weatherTemp={this.state.weatherTemp}
          forecast={this.state.forecast}
          isLoading={this.state.isLoading}
          imgSource={this.state.imgSource}
        />
      </MainContainer>
    )
  }
}

export default WeatherCityContainer