import React from 'react'
import CitySearch from './CitySearch'
import './CitySearch.scss'

function CitySearchContainer({addCityUpdate, containerClass = ''}) {
  return (
    <div className={`form-container city-form-container ${containerClass}`}>
      <CitySearch addCityUpdate={addCityUpdate}/>
    </div>
  )
}

export default CitySearchContainer