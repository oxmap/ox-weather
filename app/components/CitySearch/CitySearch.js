import React from 'react'
import FontAwesome from 'react-fontawesome'

class CitySearch extends React.Component {
  onSubmit(event) {
    event.preventDefault();
    const cityInput = document.getElementById('citySearch');
    this.props.addCityUpdate(cityInput.value);
    document.getElementById("cityForm").reset();
  }
  
  componentDidMount() {
    const input = document.getElementById('citySearch');
    new google.maps.places.Autocomplete(input, {types: ['(cities)']});
  }

  render() {
    return (
      <form id="cityForm" onSubmit={(event) => this.onSubmit(event)}>
        <div className="form-group">
          <input
            id="citySearch"
            type="text"
            className="form-control city-input"
            placeholder="Введите название города" required
          />
          <div className="icons-container">
            <FontAwesome name='search' size="lg" className="icon-search" />
            <FontAwesome name='times' size="lg" className="icon-close" />
          </div>
        </div>
      </form>
    )
  }
}

CitySearch.contextTypes = {
  router: React.PropTypes.object.isRequired
}

export default CitySearch
