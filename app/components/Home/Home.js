import React from 'react'
import MainContainer from '../Main/MainContainer'
import CitySearchContainer from '../CitySearch/CitySearchContainer'
import FontAwesome from 'react-fontawesome'
import CityCard from '../CityCard/CityCard'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { getCityCurrentWeather } from '../../helpers/apis/owm_api'
import './Home.scss'

class Home extends React.Component {
   constructor () {
    super()
    this.state = {
      citiesList: [],
      citiesWeather: []
    }
  }

  updateCities(city) {
    let myCities = this.state.citiesList.concat(city);
    localStorage.setItem('citiesList', JSON.stringify(myCities));

    this.setState({
      citiesList: myCities
    });

    this.getCityWeather(city);
    // const cityForm = document.getElementById('city-weather-element');
    // cityForm.classList.remove('form-displayed');
  }

  showAddCityForm() {
    // const cityForm = document.getElementById('city-weather-element')
    // cityForm.classList.add('form-displayed')
  }

  removeCity(city) {
    const myCities = JSON.parse(localStorage.getItem('citiesList'));
    var elementIndex = myCities.indexOf(city);
    myCities.splice(elementIndex, 1);
    localStorage.setItem('citiesList', JSON.stringify(myCities));
  }

  async getCityWeather(city) {
    const weather = await getCityCurrentWeather(city);
    if (weather === null) {
      this.removeCity(city);
      NotificationManager.warning(`Город: ${city}`, 'Данные о погоде не найдены', 4000);
      return;
    }
    const cityWeatherData = {
      cityFullName: city,
      cityName: city,
      weatherDescription: weather.data.weather[0].description,
      weatherTemp: weather.data.main.temp,
      weatherSunRise: weather.data.sys.sunrise,
      weatherSunSet: weather.data.sys.sunset,
      weatherHumidity: weather.data.main.humidity,
      weatherPressure: weather.data.main.pressure,
      weatherWindSpeed: weather.data.wind.speed,
      weatherWindDeg: weather.data.wind.deg,
      icon: weather.data.weather[0].id
    }
    this.setState({
      citiesWeather: this.state.citiesWeather.concat(cityWeatherData)
    })
  }

  componentDidMount() {
    const myCities = JSON.parse(localStorage.getItem('citiesList'))
    if (myCities) {
      myCities.map((element, i) => {
        this.getCityWeather(element)
      })
      this.setState({
        citiesList: this.state.citiesList.concat(myCities)
      })
    }
  }

  render() {
    return (
      <MainContainer pageName="home">
        <div className="container">
          <div className="center-block">
            <CitySearchContainer containerClass="header-city-form" addCityUpdate={(city) => this.updateCities(city)}/>
          </div>
        </div>
        <div id="cities-container" className="row">
          <ul>
            {this.state.citiesWeather.map(function(element, i){
              return (
                <CityCard cityData={element} key={i} removeCity={(city) => this.removeCity(city)}/>
              )
            }.bind(this))}
          </ul>
        </div>
        <NotificationContainer/>
      </MainContainer>
    )
  }
}

export default Home