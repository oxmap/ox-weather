const {resolve} = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var WebpackNotifierPlugin = require('webpack-notifier');
const Dotenv = require('dotenv-webpack');

module.exports = function () {
    return {
        context: resolve(__dirname, './../app'),

        entry: {
            vendor: ['react', 'react-dom'],
            app: './index.js'
        },

        module: {
            rules: [
                {
                    test: /\.js$/,
                    use: ['babel-loader'],
                    exclude: /node_modules/
                },
                {
                    test: /\.(css|scss)$/,
                    use:[
                      'style-loader',
                      'css-loader', 
                      'sass-loader'
                    ]
                },
                {
                    test: /\.(ico|svg|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2)(\?.*)?$/,
                    use: 'file-loader?name=img/[name]_[hash:5].[ext]',
                }
            ]
        },

        plugins: [
            new HtmlWebpackPlugin({
                title: 'Weather App',
                // favicon: resolve(__dirname, './../app/assets/favicon.ico'),
                favicon: './../app/assets/favicon.ico',
                template: __dirname + '/template.html'
            }),
            new WebpackNotifierPlugin(),
            new Dotenv({
              path: './.env', // Path to .env file (this is the default)
              systemvars: true
            })
        ]
    }
}